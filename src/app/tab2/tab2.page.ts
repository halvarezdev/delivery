import { Component } from '@angular/core';
import {  ToastController } from '@ionic/angular';
import { PostProvider } from '../../providers/post-provider';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  //limite de datos
  anggota: any;
  app_vendedor_id:any;
  app_estado:any = '0';
  reports: any = [];
  kpis: any = [];
  limit: number = 50; // LIMIT GET PERDATA
  start: number = 0;
  constructor(
    public toastController: ToastController,
    private PostProvider: PostProvider,
    private storage: Storage,
   
  ) {

    this.storage.get('session_storage').then((res) => {
      this.anggota = res;
      this.app_vendedor_id = this.anggota.app_vendedor_id;
      });
  }

  ionViewWillEnter(){
  	this.reports = [];
    this.kpis = [];
    this.start = 0;
    this.loadReports();
    this.loadKpis();
  }


  doRefresh(event){
  	setTimeout(() =>{
  		this.ionViewWillEnter();
      event.target.complete();
     	}, 500);
  }

  loadData(event: any){
    this.start += this.limit;
   	setTimeout(() =>{
  	this.loadReports().then(()=>{
      event.target.complete();
   
  	});

    this.loadKpis().then(()=>{
      event.target.complete();
   
  	});
  	}, 500);
  }


  loadReports(){
  	return new Promise(async resolve => {
  		let body = {   
        send : 'reporte',  
   			limit : this.limit,
        start : this.start,
        app_vendedor_id: this.app_vendedor_id
      };

  		(await this.PostProvider.postData(body, 'proses-api.php')).subscribe(async(res:any) => {
  			for(let report of res.result){
          
  				this.reports.push(report);
  			}
        resolve(true);
      });
  	});
  }

  loadKpis(){
  	return new Promise(async resolve => {
  		let body = {   
        send : 'kpis',  
   			limit : this.limit,
        start : this.start,
        app_vendedor_id: this.app_vendedor_id
      };

  		(await this.PostProvider.postData(body, 'proses-api.php')).subscribe(async(res:any) => {
  			for(let kpi of res.total){
          
  				this.kpis.push(kpi);
  			}
        resolve(true);
      });
  	});
  }


eliminar(app_delivery_id){
	return new Promise(async resolve => {
    let body = {
      send : 'eliminar',
      app_estado: this.app_estado,
      app_delivery_id : app_delivery_id
     
    };
    console.log(body);

    (await this.PostProvider.postData(body, 'proses-api.php')).subscribe(async(res:any) => {
      var alertpesan = res.msg;
      if (res.success) {
        const toast = await this.toastController.create({
          message: alertpesan,
          duration: 500
        });
        toast.present();
        this.ionViewWillEnter();
      }else {
        const toast = await this.toastController.create({
          message: alertpesan,
          duration: 500
        });
        toast.present();
      }
      resolve(true);
    });
  });
}

}
