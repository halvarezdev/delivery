import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';
import { PostProvider } from '../../providers/post-provider';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  dni: string;
  password: string;


  constructor(
    private router: Router,
    private PostProvider: PostProvider,
    private storage: Storage,
    public toastCtrl: ToastController,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
  }

  async prosesLogin() {

    if (this.dni != "" && this.password != "") { 

      const loading = await this.loadingController.create({
        spinner: 'circles',
        message: 'Loading...',
        duration: 1000
      });

      let body = {
        dni: this.dni,
        password: this.password,
        send: 'login'
      }

      await loading.present();
      (await (this.PostProvider.postData(body, 'proses-api.php'))).subscribe(async(res:any) => {
              var alertpesan = res.msg;
              if (res.success) {
                loading.dismiss();
                this.storage.set('session_storage', res.result);
                this.router.navigate(['/tabs/tab1']);
                const toast = await this.toastCtrl.create({
                  message: 'Bienvenido ' + this.dni,
                  duration: 2000
                }); toast.present();
                this.dni = '';
                this.password='';
              } else {
                const toast = await this.toastCtrl.create({
                  message: alertpesan,
                  duration: 2000
                }); toast.present();
              }
      },async () => {
          loading.dismiss();
          const toast = await this.toastCtrl.create({
            message: 'No es posible conectarse al servidor',
            duration: 2000,

          });toast.present();
        });
  }else{
    async data =>{
    var alertpesan = data.msg;
    this.loadingController.dismiss();
      const toast = await this.toastCtrl.create({
        message: alertpesan,
        duration: 2000,
      }); toast.present();}
  }
}




}
