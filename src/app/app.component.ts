import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {


  constructor(

    private router: Router,
    private storage: Storage,
  ) {


  }

  async ngOnInit() {

    await this.storage.create();
    this.storage.get('session_storage').then((res)=>{ 
      if(res==null){
        this.storage.clear();
        this.router.navigate(['/login']);
      }else
      this.router.navigate(['/tabs/tab1']);

    });
  }





}