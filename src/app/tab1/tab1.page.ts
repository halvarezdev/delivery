import { Component } from '@angular/core';
import {Router } from '@angular/router';
import {  ToastController } from '@ionic/angular';

import { PostProvider } from '../../providers/post-provider';
import { Storage } from '@ionic/storage';

interface MediosPago {
  id: number;
  medio_pago: string;
 }
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page {
  fecha: string;
  hora: string;
  minutos: string;
  segundos: string;
  anggota: any;
  app_vendedor_id: any;
  fullname: any;
  name: string;
  direccion: string;
  celular: string;
  ganancia_vendedor: any;
  ganancia_total: any;
  numero_pedido: any;
  pago:string;
  wsp:string = 'https://wa.me/51';
  texto:string = '?text=Buenas%20noches%20señor@,%20soy%20Liseth%20su%20delivery%20de%20PapeOS.%20Me%20podría%20compartir%20su%20ubicación%20actual%20por%20este%20medio%20para%20ir%20exacto%20a%20su%20dirección.%20Le%20agradecería%20bastante.%20Muchas%20gracias%20(^‿^)'
  maps:string = 'https://www.google.com/maps/search/';
  
  constructor(
    private router: Router,
    public toastController: ToastController,
    private PostProvider: PostProvider,
    private storage: Storage,
    public toastCtrl: ToastController,


  ) {

  }

 

  ngOnInit() {
    
    this.timeout();
    this.fecha = this.fechaNow();
    this.storage.get('session_storage').then((res) => {
      this.anggota = res;
      this.app_vendedor_id = this.anggota.app_vendedor_id;
      this.fullname = this.anggota.fullname;
      });
     


  }

  timeout() {
    setTimeout(() => {
      this.fecha = this.fechaNow();
       this.timeout();
    }, 1000);
  }
  fechaNow() {//Fecha
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); 
    var yyyy = today.getFullYear();
    //
    var horas = new Date();
    var hour = String(horas.getHours()).padStart(2, '0');
    //
    var minute = new Date();
    var minutes = String(minute.getMinutes()).padStart(2, '0');
    //
    var seg = new Date();
    var seconds = String(seg.getSeconds()).padStart(2, '0');
    return this.fecha = yyyy + "-" + mm + "-" + dd + " " + hour + ":" + minutes + ":"+seconds;
  }


  async registrar() {
    // validation done
    if (this.name == null||this.direccion == null||this.celular ==null||this.ganancia_total ==null||this.pago==null) {
      const toast = await this.toastController.create({
        message: 'Todos los campos son requeridos!',
        duration: 3000
      });
      toast.present();
    } else {

      let body = {

        app_name : this.name,
        app_direccion : this.direccion,
        app_celular : this.celular,
        app_wsp : this.wsp.concat(this.celular,this.texto),
        app_maps : this.maps.concat(this.direccion),
        app_ganancia_vendedor : this.ganancia_total/2,
        app_ganancia_total : this.ganancia_total,
        app_numero_pedido: this.numero_pedido,
        app_vendedor_id : this.app_vendedor_id,
        app_fecha : this.fecha,
        app_medio_pago: this.pago,
        send: 'registrar'
      };
      console.log(body);
      (await (this.PostProvider.postData(body, 'proses-api.php'))).subscribe(async(res:any) => {
        var alertpesan = res.msg;
        if (res.success) {
          this.name = '';
          this.direccion = '';
          this.celular = '';
          this.ganancia_vendedor = '';
          this.ganancia_total = '';
          this.numero_pedido = '';
          this.pago='';
          this.wsp = 'https://wa.me/51';
          this.maps= 'https://www.google.com/maps/search/';
          this.router.navigate(['/tabs/tab2']);
          const toast = await this.toastController.create({
            message: alertpesan,
            duration: 500
          });
          toast.present();
        } else {
          const toast = await this.toastController.create({
            message: alertpesan,
            duration: 500
          });
          toast.present();
        }
      });

    }

  }

  async prosesLogout() {
    this.storage.clear();
    this.router.navigate(['/login']);
    const toast = await this.toastCtrl.create({
      message: 'Sesión Cerrada correctamente',
      duration: 3000
    });
    toast.present();
  }

  mediospago: MediosPago[] = [
    {
      id: 1,
      medio_pago: 'Efectivo'
    },
    {
      id: 2,
      medio_pago: 'Yape!'
    },{
      id: 3,
      medio_pago: 'Plin'
    },{
      id: 4,
      medio_pago: 'Transferencia'
    },{
      id: 5,
      medio_pago: 'Visa'
    },
  ];

  compareWith(o1: MediosPago, o2: MediosPago) {
    
    return o1 && o2 ? o1.id === o2.id : o1 === o2;

  }

  onChangue(event) {
    this.pago= event.target.value ;
    
  }


}
