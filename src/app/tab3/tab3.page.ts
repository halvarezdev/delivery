import { Component} from '@angular/core';
import {  ToastController } from '@ionic/angular';
import { PostProvider } from '../../providers/post-provider';
import { Storage } from '@ionic/storage';



@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page  {

  anggota: any;
  app_vendedor_id:any;
  app_estado:any = '0';
  historial: any = [];
  total_historial: any = [];
  final: any;
  inicial:any;


  constructor(
    public toastController: ToastController,
    private PostProvider: PostProvider,
    private storage: Storage,
   
  ) {

    this.storage.get('session_storage').then((res) => {
      this.anggota = res;
      this.app_vendedor_id = this.anggota.app_vendedor_id;
      });
  }

  ionViewWillEnter(){
  	this.historial = [];
    this.total_historial = [];
   // this.start = 0;
    this.loadHistorial();
    this.loadTotalHistorial();
  
    //console.log(this.inicial,this.final)
  }


  doRefresh(event){
  	setTimeout(() =>{
  		this.ionViewWillEnter();
     
      event.target.complete();
     	}, 500);
  }

  loadData(event: any){
    //this.start += this.limit;
   	setTimeout(() =>{
  	this.loadHistorial().then(()=>{
      event.target.complete();
   
  	});

    this.loadTotalHistorial().then(()=>{
      event.target.complete();
   
  	});
  	}, 500);
  }


  async loadHistorial(){
  	return new Promise(async resolve => {
      let fecha_inicial = this.inicial.split('T')[0]; 
      let fecha_final = this.final.split('T')[0]; 
     	let body = {   
        send : 'historial',  
        app_fecha_inicial: fecha_inicial,
        app_fecha_final: fecha_final,
        app_vendedor_id: this.app_vendedor_id
      };
      console.log(body);

  		(await this.PostProvider.postData(body, 'proses-api.php')).subscribe(async(res:any) => {
  			for(let historial of res.results){
          
  				this.historial.push(historial);
  			}
        resolve(true);
      });
  	});
  }

  async loadTotalHistorial(){
  	return new Promise(async resolve => {
      let fecha_inicial = this.inicial.split('T')[0]; 
      let fecha_final = this.final.split('T')[0]; 
  		let body = {   
        send : 'total_historial',  
        app_fecha_inicial: fecha_inicial,
        app_fecha_final: fecha_final,
        app_vendedor_id: this.app_vendedor_id
      };
      console.log(body);
  		(await this.PostProvider.postData(body, 'proses-api.php')).subscribe(async(res:any) => {
  			for(let total_historial of res.total_historial){
          
  				this.total_historial.push(total_historial);
  			}
        resolve(true);
      });
  	});
  }


}
    