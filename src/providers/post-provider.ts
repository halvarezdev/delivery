import { Injectable } from '@angular/core';
import {HttpClient,  HttpHeaders } from '@angular/common/http';
import  'rxjs/add/operator/map';


@Injectable()

export class PostProvider {

	
	server: string = "http://35.184.242.69/api/";
	//server: string = "http://localhost:8080/api/";
	
	constructor(public http: HttpClient) {}


	
	async postData(body, file){
	

			const headeer = new HttpHeaders({
				'Content-Type' : 'application/json; charset=UTF-8'
			});

			const options =  {
				
				headers : headeer
			
			};
			
			return this.http.post(this.server + file, JSON.stringify(body), options).map(res => (res));
						
	}
}